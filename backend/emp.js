const express = require('express')
const utils = require('./utils')
const db1 = require('./db')

const router = express.Router()

router.get('/', (req, res) => {
    const stmt = `select * from Emp`
    db1.execute(stmt,(error,result)=>{
        res.send(utils.createResult(error,result))
    })
       
})

router.post('/', (req, res) => {
    const { name, salary, age } = req.body
    const stmt = `insert into Emp (name,salary,age) values ('${name}','${salary}','${age}')`
    db1.execute(stmt,(error,result)=>{
        res.send(utils.createResult(error,result))
    })
})

module.exports = router
