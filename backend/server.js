const express = require('express')
const routerEmp = require('./emp')
const app = express()

app.use(express.json())
app.use('/emp',routerEmp)


app.listen(4000, ()=>{
    console.log('server started at port : 4000')
})