create table Emp (
    empid int primary key auto_increment,
    name varchar(100),
    salary float,
    age int
);

insert into Emp (name,salary,age) values ('Aditya',33400,210);